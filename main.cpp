#include <iostream>
#include <string>
#include <bits/stdc++.h>
#include<queue>
#include<stack>
#include<stdlib.h>


#define MAX 100
#define V 5
#define INF 99999
using namespace std;

int cont_chof=0,
    cont_meca=0,
    cont_av=0,
    cont_dir=0,
    cont_back=0,
    cont_vehiculo=0,
    cont_cliente=0;

int tabla[MAX][MAX];
int solucion[MAX];
int grafo[V][V];
struct persona{
    int dni;
    string nombre;
    string apellido;
};
struct conductor{
    int num_conductor;
    persona per;
    string vehiculo;
};
struct mecanico{
    int num_mecanico;
    persona per;
    int experiencia[100];
    string estado;
};
struct vehiculo{
    int codigo;
    string placa;
    int estado;
};
struct calles{
    int codigo;
    string nombre;
};
struct averia{
    int codigo;
    string nombre;
};
struct cliente{
    string nombre;
    string clase;
    int codigo;
};
void cabecera();
void binaria(mecanico bm[],int dato){
    int inf,sup,mitad;
    char band = 'F';

    inf = 0;
    sup = 5;

    while(inf <= sup){
        mitad = (inf+sup)/2;
        if(bm[mitad].per.dni==dato){
            band='V';
            break;
        }
        if(bm[mitad].per.dni > dato){
            sup = mitad;
            mitad = (inf+sup)/2;
        }
        if(bm[mitad].per.dni < dato){
            inf = mitad;
            mitad = (inf+sup)/2;
        }
    }
    if(band=='V'){
        cabecera();
        cout<<bm[mitad].num_mecanico<<"\t\t"<<bm[mitad].per.dni
        <<"\t\t"<<bm[mitad].per.nombre<<"\t\t"<<bm[mitad].per.apellido<<"\t\t"<<bm[mitad].estado;
    }else{
        cout<<"El dni indicado no existe"<<endl;
    }
}
void ordenamiento(mecanico bm[]){
    int i,j;
    mecanico aux;
    for(i=0;i<5;i++){
        for(j=0;j<5;j++){
            if(bm[j].per.dni > bm[j+1].per.dni){
                aux = bm[j];
                bm[j] = bm[j+1];
                bm[j+1] = aux;
            }
        }
    }
}
void printSolution(int dist[][V], calles cll[]);
void mostrarPila(stack <cliente> s) {
    stack<cliente> aux;
    while (!s.empty()){
        cout << '\t' << s.top().nombre;
        aux.push(s.top());
        s.pop();
    }
    while (!aux.empty()){
        s.push(aux.top());
        aux.pop();
    }
    cout << '\n';
}
void back(int i,int n,stack<cliente> pila,queue<cliente> cola,int tam){
    if(i>=tam){
        cont_back++;
        cout<<cont_back<<". ";
        mostrarPila(pila);
    }
    else{
        for(int j=i;j<n;j++){

            if(!((i==0 and cola.front().clase =="B") or
                 (i==0 and cola.front().clase =="C"))){
                pila.push(cola.front());
                cola.pop();

                back(i+1,n,pila,cola,tam);
            }
            if (!pila.empty()){
                cola.push(pila.top());
                pila.pop();
            }
        }
    }
}
void floydWarshall(int graph[][V],calles cll[]){

    int dist[V][V], i, j, k;
    for (i = 0; i < V; i++)
        for (j = 0; j < V; j++)
            dist[i][j] = graph[i][j];
    for (k = 0; k < V; k++) {
        for (i = 0; i < V; i++) {
            for (j = 0; j < V; j++) {
                if (dist[i][j] > (dist[i][k] + dist[k][j])
                    && (dist[k][j] != INF
                        && dist[i][k] != INF))
                    dist[i][j] = dist[i][k] + dist[k][j];
            }
        }
    }
    printSolution(dist,cll);
}
void printSolution(int dist[][V], calles cll[])
{
    cout << "Matriz de distancia más corta \n";
    for (int i = 0; i < V; i++) {
        for (int j = 0; j < V; j++) {
            if (dist[i][j] == INF)
                cout << "INF"
                     << "	 ";
            else
                cout << dist[i][j] << "	 ";
        }
        cout << endl;
    }
}
void cabecera(){
    std::cout<<"---------------------------------------------------------------------------";
    std::cout<<"\t\t\t\t\tDatos";
    std::cout<<endl<<"---------------------------------------------------------------------------"<<endl;
    std::cout<<endl<<"CODIGO\t\tDNI\t\t\tNOMBRE\t\tAPELLIDO";
    std::cout<<endl<<"---------------------------------------------------------------------------"<<endl;
}
void regDirecciones(calles cll[]){
    char rpta;
    do{
        cll[cont_dir].codigo=cont_dir;
        std::cout<<"Nombre de Ubicacion: ";
        std::cin.ignore();
        std::getline(cin,cll[cont_dir].nombre);
        std::cout<<"\nDesea registrar otra ubicacion s=si, n=no: ";
        cin>>rpta;
        cont_dir++;
    }while(rpta == 's');
}
void verDirecciones(calles cll[]){
    if(cont_dir==0){
        cout<<"Sin datos para mostrar, inserte datos";
    }else{
        cout<<"\nCodigo\t\tNombre\n";
        for (int i = 0; i < cont_dir; ++i) {
            cout<<cll[i].codigo<<"\t\t"<<cll[i].nombre<<endl;
        }
    }
}
void regDistancias(calles cll[]){
    if (cont_dir==0){
        cout<<"Por favor primero registre ciudades: ";
    }else{
        verDirecciones(cll);
        cout<<"\nIngrese las distancias entre puntos y puntos, de no existir distancias ingrese 0\n";
        for (int i = 0; i < cont_dir; ++i) {
            for (int j = 0; j < cont_dir; ++j) {
                if (i == j){
                    grafo[i][j]=0;
                } else{
                    cout<<"distancia entre "<<cll[i].nombre<<" y "<<cll[j].nombre<<": ";
                    cin>>grafo[i][j];
                    if ((grafo[i][j]==0) and (i!=j)){
                        grafo[i][j]=INF;
                    }
                }
            }
        }
    }
}
void rutaMinima(calles cll[]){

    if (cont_dir==0){
        cout<<"\nSin datos donde evaluar ruta minima\n";
    }else{
        verDirecciones(cll);
        floydWarshall(grafo,cll);
    }
}
void limpiarEstado(mecanico mm[]){
    for (int i = 0; i < cont_meca; ++i) {
        mm[i].estado="libre";
    }
}
void voraz(mecanico mm[],averia av[]){
    int mecanico = 0;
    int averia = 0;
    while ((mecanico<cont_meca) and (averia<cont_av)){
        if (tabla[mecanico][averia]==0 || mm[mecanico].estado == "ocupado"){
            mecanico++;
            if(mecanico==cont_meca){
                averia++;
                mecanico=0;
            }
        } else{
            solucion[averia]=mecanico;
            mm[mecanico].estado = "ocupado";
            averia++;
            mecanico=0;
        }
    }
}
void SolucionAveria(averia av[], mecanico mm[]){
    limpiarEstado(mm);
    for(int i = 0; i<cont_meca; i++){
        for(int j = 0; j<cont_av; j++){
            tabla[i][j] = mm[i].experiencia[j];
        }
    }
    for (int i = 0; i < cont_av; ++i) {
        solucion[i] = -1;
    }
    voraz(mm,av);
    for (int i = 0; i < cont_av; ++i) {
        if (solucion[i] != -1){
            std::cout<<"La averia "<<av[i].nombre<<" sera solucionado por el mecanico "<< mm[solucion[i]].per.nombre<<endl;
        }else{
            std::cout<<"La averia "<<av[i].nombre<<" No se puede solucionar por ninguno de nuetsros mecanicos"<<endl;
        }
    }
}
void modificarMecanic(mecanico bm[],int dni,int n, averia av[]){
    char flag = false;
    int i = 0,op;
    char rpta;
    while ((flag == false) && (i < n)){
        if(bm[i].per.dni==dni){
            flag = true;
        }
        i++;
    }
    if(flag == false){
        cout<<"El dni insertado no existe\n";
    }else if (flag==true){
        cabecera();
        cout<<bm[i-1].num_mecanico<<"\t\t"<<bm[i-1].per.dni<<"\t\t"<<bm[i-1].per.nombre<<"\t\t"<<bm[i-1].per.apellido;
        cout<<endl;
        do{
            cout<<"1 - Modificar DNI\n";
            cout<<"2 - Modificar Nombre\n";
            cout<<"3 - Modificar Apellido\n";
            cout<<"4 - modificar experiencias\n";
            cout<<"0 - Regresar\n";
            cout<<"Elija opcion: ";
            cin>>op;
            switch (op) {
                case 1:{
                    cout<<"Ingrese nuevo Dni: ";
                    cin>>bm[i-1].per.dni;
                }
                    break;
                case 2:{
                    cout<<"Ingrese nuevo nombre: ";
                    cin>>bm[i-1].per.nombre;
                }
                    break;
                case 3:{
                    cout<<"Ingrese nuevo apellido: ";
                    cin>>bm[i-1].per.apellido;
                }
                    break;
                case 4:{
                    for (int j = 0; j <cont_av ; ++j) {
                        cout<<"Experiencia para averia  "<<av[j].nombre<<": ";
                        cin>>bm[i-1].experiencia[j];
                    }
                }
            }
            cout<<"Desea modificar otro campo? s=si, n=no: ";
            cin>>rpta;
        }while(rpta == 's');
    }
}
void buscarMecanico(mecanico bm[],int dni,int n){
    ordenamiento(bm);
    binaria(bm,dni);
}
void buscarChofer(conductor bc[],int dni,int n){
    char flag = false;
    int i = 0;
    while ((flag == false) && (i < cont_chof)) {
        if (bc[i].per.dni == dni) {
            flag = true;
        }
        i++;
    }
    if(flag == false){
        cout<<"El dni insertado no existe\n";
    }else if (flag==true){
        cabecera();
        cout<<bc[i-1].num_conductor<<"\t\t"<<bc[i-1].per.dni<<"\t\t"<<bc[i-1].per.nombre<<"\t\t"<<bc[i-1].per.apellido;
        cout<<endl;

    }
}
template <typename Y>
void intercambio(Y &x, Y &y){
    Y aux;
    aux = x;
    x = y;
    y = aux;
}
template <typename T>
void quickSort(T cc[],int inicial, int ultimo){
    int i = inicial;
    int j = ultimo;
    int central = (inicial+ultimo)/2;
    int pivote = cc[central].per.dni;

    do{
        while(cc[i].per.dni<pivote) i++;
        while(cc[j].per.dni>pivote) j--;

        if(i<=j){
            intercambio(cc[i],cc[j]);
            i++;
            j--;
        }

    }while(i<=j);

    if(inicial<j){
        quickSort(cc,inicial,j);//llamar al algorito para order la sublita izquierda
    }if(i<ultimo){
        quickSort(cc,i,ultimo);//llamar al algorito para order la sublita derecha
    }
}
void  MostrarMecanic(mecanico mm[],averia av[]){
    int dni_mod = 0;
    quickSort(mm, 0, cont_meca - 1);
    cabecera();
    for (int i = 0; i < cont_meca; ++i) {
        cout<<mm[i].num_mecanico<<"\t"<<mm[i].per.dni<<"\t"
        <<mm[i].per.nombre<<"\t\t"<<mm[i].per.apellido<<"\t\t"<<mm[i].estado;
        for (int j = 0; j < cont_av; ++j) {
            cout<<"\tExperiancia "<<mm[i].experiencia[j];
        }
        cout<<endl;
    }
    cout<<"Ingrese el dni del mecanico a modificar o en su defecto ingrese 0 para salir";
    cin>>dni_mod;
    if (dni_mod!=0){
        modificarMecanic(mm,dni_mod,cont_meca,av);
    }
    limpiarEstado(mm);
}
void  MostrarChoferes(conductor cc[]){
    quickSort(cc, 0, cont_chof - 1);
    cabecera();
    for (int i = 0; i < cont_chof; ++i) {
        cout<<cc[i].num_conductor<<"\t\t"<<cc[i].per.dni
        <<"\t\t"<<cc[i].per.nombre<<"\t\t"<<cc[i].per.apellido<<"\t\t"<<cc[i].vehiculo;
        cout<<endl;
    }
}
void insert_mecanico(mecanico mm[],averia av[]){
    if(cont_av==0){
        cout<<"¡Primero debe de inseratr averias!"<<endl;
    }else{
        char rpta = 's';
        while(rpta=='s'){
            std::cout<<"*****Datos de registro del chofer*****\n";
            mm[cont_meca].num_mecanico = cont_meca + 1;
            std::cout<<"DNI: ";
            cin>>mm[cont_meca].per.dni;
            std::cout<<"Nombre: ";
            cin.ignore();
            getline(std::cin,mm[cont_meca].per.nombre);
            std::cout<<"Apellido: ";
            getline(std::cin,mm[cont_meca].per.apellido);
            cout<<"Ingrese la experiencia en las diferentes averias (sin expe = 0)"<<endl;
            for (int i = 0; i <cont_av ; ++i) {
                cout<<"Experiencia para averia  "<<av[i].nombre<<": ";
                cin>>mm[cont_meca].experiencia[i];
            }
            mm[cont_meca].estado="libre";
            std::cout<<"¿Desea ingresar otro mecanico? (s = si, n = no): ";
            std::cin>>rpta;
            cont_meca ++;
        }
    }
}
void insert_chofer(conductor cc[]){
    char rpta = 's';
    while(rpta=='s'){
        std::cout<<"*****Datos de registro del chofer*****\n";
        cc[cont_chof].num_conductor = cont_chof + 1;
        std::cout<<"DNI: ";
        cin>>cc[cont_chof].per.dni;
        std::cout<<"Nombre: ";
        cin.ignore();
        getline(std::cin,cc[cont_chof].per.nombre);
        std::cout<<"Apellido: ";
        getline(std::cin,cc[cont_chof].per.apellido);
        std::cout<<"¿Desea ingresar otro chofer?: ";
        std::cin>>rpta;
        cc[cont_chof].vehiculo="s/v Asignado";
        cont_chof ++;
    }
}
void insertarAveria(averia av[]){
    char re;
    do{
        av[cont_av].codigo=cont_av + 1;
        cout<<"Ingrese nombre de averia: ";
        cin.ignore();
        getline(cin,av[cont_av].nombre);
        cont_av++;
        cout<<"Desea ingresar otra averia?: (s=si, n = no): ";
        cin>>re;
    } while (re=='s');
}
void regVehiculo(vehiculo vh[]){
    char resp;
    do{
        vh[cont_vehiculo].codigo=cont_vehiculo;
        std::cout<<"Ingrese Placa del carro: ";
        cin.ignore();
        getline(cin,vh[cont_vehiculo].placa);
        vh[cont_vehiculo].estado=rand()%2;
        std::cout<<"Desea ingresar otr vehiculo s = si, n = no: ";
        cin>>resp;
        cont_vehiculo++;
    }while(resp == 's');
}
void AsignarVehi(vehiculo vh[],conductor cc[]){
    MostrarChoferes(cc);
    cout<<"\nLista de Vehiculos\n";
    for (int i = 0; i < cont_vehiculo; ++i) {
        cout<<vh[i].codigo<<"\t\t"<<vh[i].placa<<endl;
    }
    cout<<"\nIngrese el numero de placa de vehiculo asignado al chofer\n";
    for (int i = 0; i < cont_vehiculo; ++i) {
        cout<<"vehicsulo para "<<cc[i].per.nombre<<": ";
        cin.ignore();
        getline(cin,cc[i].vehiculo);
    }
}
void listarAverias(averia av[]){
    for (int i = 0; i < cont_av; ++i) {
        cout<<av[i].codigo<<" - "<<av[i].nombre<<endl;
    }
}
void menu();
void menuCliente(vehiculo vh[],cliente cl[]){

    queue<cliente> cola;
    stack<cliente> pila;
    int num_clientes,cont=0;
    cout<<"\nLista de Vehiculos\n";
    for (int i = 0; i < cont_vehiculo; ++i) {
        cout<<vh[i].codigo<<"\t\t"<<vh[i].placa<<"\t\t"<<vh[i].estado<<endl;
        if(vh[i].estado==0){
            cont++;//la cantida de vehiculos libres
        }
    }
    cout<<"\ningrese el numero de clientes que desean servicio: ";
    cin>>num_clientes;
    for (int i = 0; i < num_clientes; ++i) {

        cout<<"\ningrese nombre de cliente "<<i+1<<": ";
        cin>>cl[i].nombre;
        cout<<"Tipo de cliente: ";
        cin>>cl[i].clase;
        cola.push(cl[i]);
    }
    int i=0;
    back(i,cola.size(),pila,cola,cont);
    menu();

}
void menuChoferes(conductor cc[]){
    char rptaChofer;
    int opChofer,dni_buscar;
    do{
        std::cout<<"\n\tMenu Choferes\n";
        std::cout<<"\n---------------------------------\n";
        std::cout << "1. Insertar Chofer\n";
        std::cout << "2. Listar Choferes\n";
        std::cout << "3. Buscar chofer\n";
        std::cout << "0. Regresar al menu Principal\n";
        std::cout<<"<---------------------------------\n";
        std::cout << "Ingrese opcion: ";
        std::cin >> opChofer;
        switch (opChofer) {
            case 1: insert_chofer(cc);
                break;
            case 2: MostrarChoferes(cc);
                break;
            case 3:{
                cout<<"Ingrese el dni a buscar: ";
                cin>>dni_buscar;
                buscarChofer(cc,dni_buscar,cont_chof);
            }
            case 0: menu();
                break;
        }
        cout<<"\nDesea continuar en el menu choferes (s = si, r = regresar): ";
        cin>>rptaChofer;
    }while(rptaChofer=='s');
}
void menuTallerMecanico(mecanico mm[],averia av[]){
    char rptaMeca;
    int opMeca,dni_buscar;
    do{
        std::cout<<"\n\tMenu Mecanicos\n";
        std::cout<<"\n---------------------------------\n";
        std::cout << "1. Insertar Mecanico\n";
        std::cout << "2. listar Mecanicos\n";
        std::cout << "3. Buscar Mecanico\n";//se realiza la busqueda con busqueda binaria
        std::cout << "4. Agregar Averia\n";
        std::cout << "5. listar Averias\n";
        std::cout << "6. Buscar Solucion para averia\n";
        std::cout << "0. Regresar al menu Principal\n";
        std::cout<<"<---------------------------------\n";
        std::cout << "Ingrese opcion: ";
        std::cin >> opMeca;
        switch (opMeca) {
            case 1: insert_mecanico(mm,av);
                break;
            case 2: MostrarMecanic(mm,av);
                break;
            case 3:{
                cout<<"Ingrese el dni a buscar: ";
                cin>>dni_buscar;
                buscarMecanico(mm,dni_buscar,cont_chof);
            }
                break;
            case 4:
                insertarAveria(av);
                break;
            case 5:
                listarAverias(av);
                break;
            case 6:
                SolucionAveria(av,mm);
                break;
            case 0: menu();
                break;
        }
        cout<<"\nDesea continuar en el menu Taller mecanico (s = si, r = regresar): ";
        cin>>rptaMeca;
    }while(rptaMeca=='s');

}
void menuMapa(calles cll[]){
    int op;
    char  resp;
    do{
        std::cout<<"1 - Registrar Direcciones\n";
        std::cout<<"2 - Registrar Distancias\n";
        std::cout<<"3 - Consultar Ruta minima\n";
        std::cout<<"0 - Regresar\n";
        std::cout<<"Ingrese opcion: ";
        cin>>op;
        switch (op) {
            case 1:regDirecciones(cll);
                break;
            case 2:regDistancias(cll);
                break;
            case 3: rutaMinima(cll);
                break;
        }
        std::cout<<"Desea continuar en el menu Mapa s = si, n = n: ";
        cin>>resp;
    }while(resp=='s');
}
void menuVehiculo(vehiculo vh[],conductor cc[]){
    int op;
    char  resp;
    do{
        std::cout<<"1 - Registrar Vehiculo\n";
        std::cout<<"2 - Asignar vehiculo a chofer\n";
        std::cout<<"0 - Regresar\n";
        std::cout<<"Ingrese opcion: ";
        cin>>op;
        switch (op) {
            case 1:
                regVehiculo(vh);
                break;
            case 2:AsignarVehi(vh,cc);
                break;
            case 0:
                break;
        }
        std::cout<<"Desea continuar en el menu Vebhiculos s = si, n = n: ";
        cin>>resp;
    }while(resp=='s');
}
void menu(){
    char rpta;
    conductor* cc = new conductor[100];
    mecanico* mm = new mecanico[100];
    averia* av = new averia[100];
    calles* cll = new calles[100];
    vehiculo* vh = new vehiculo[100];
    cliente* cl = new cliente[100];
    do {
        int op;
        std::cout<<"\n\tMenu Principal\n";
        std::cout<<"---------------------------------\n";
        std::cout << "1. Modulo Choferes\n";
        std::cout << "2. Modulo Mecanico\n";
        std::cout << "3. Modulo direcciones\n";
        std::cout << "4. Modulo Vehiculos\n";
        std::cout << "5. Modulo Cliente\n";
//std::cout << "3. Modulo Averias\n";
        std::cout << "0. Salir\n";
        std::cout<<"---------------------------------\n";
        std::cout << "Ingrese opcion: ";
        std::cin >> op;
        switch (op) {
            case 1:menuChoferes(cc);
                break;
            case 2:menuTallerMecanico(mm,av);
                break;
            case 3: menuMapa(cll);
                break;
            case 4:menuVehiculo(vh,cc);
                break;
            case 5: menuCliente(vh,cl);
                break;
            case 0:{
                rpta='n';
                cout<<"***Good Bye***";
            }
                break;
            default:
                cout << "Opcion incorrecta";
        }
        if (op!=0){
            std::cout << "\nDesea continuar en el Menu Principal (s=si,n=no): ";
            std::cin >> rpta;
        }
    }while(rpta=='s');
}
int main() {

   menu();
}
